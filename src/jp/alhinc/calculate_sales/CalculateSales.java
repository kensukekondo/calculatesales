package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String NOT_SERIAL_NUMBER = "売上ファイルが連番になっていません";
	private static final String AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String NO_BRANCH_NUMBER = "の支店コードが不正です";
	private static final String NO_COMMODITY_NUMBER = "の商品コードが不正です";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されているか確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店定義ファイル")) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[a-zA-Z0-9]{8}$", "商品定義ファイル")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();

		//ファイルのリストを作成
		List<File> rcdFiles = new ArrayList<>();

		//ファイル名を取得し必要なファイルを保持
		for (int i = 0; i < files.length; i++) {

			//数字8桁.rcdファイルなのか確認
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}\\.rcd$")) {

				//数字8桁.rcdの売上ファイルを保持
				rcdFiles.add(files[i]);
			}
		}

		//Listをソート
		Collections.sort(rcdFiles);

		//売上ファイルが連番か確認
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		BufferedReader br = null;

		//売上ファイルの数だけ繰り返す
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				List<String> fileContents = new ArrayList<>();

				String line;

				//一行づつ読み込む
				while ((line = br.readLine()) != null) {
					fileContents.add(line);
				}

				//売上ファイルのフォーマット確認
				if (fileContents.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_FORMAT);
					return;
				}

				//ファイル名の取得
				String rcdFileName = rcdFiles.get(i).getName();

				//Mapに特定のKeyが存在するか確認
				if (!branchNames.containsKey(fileContents.get(0))) {
					System.out.println(rcdFileName + NO_BRANCH_NUMBER);
					return;
				}

				//Mapに特定のKeyが存在するか確認
				if (!commodityNames.containsKey(fileContents.get(1))) {
					System.out.println(rcdFileName + NO_COMMODITY_NUMBER);
					return;
				}

				//売上額が数字なのかを確認
				if (!fileContents.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//売上額の型変換
				long fileSale = Long.parseLong(fileContents.get(2));

				//支店別に売上額を加算
				Long saleAmount = branchSales.get(fileContents.get(0)) + fileSale;

				//商品別に売上金額を加算
				Long productAmount = commoditySales.get(fileContents.get(1)) + fileSale;

				//branchSalesに支店コードと売上額を保持
				branchSales.put(fileContents.get(0), saleAmount);

				//commoditySalesに商品コードと売上額を合計
				commoditySales.put(fileContents.get(1), productAmount);

				//支店別売上額の合計が10桁を超えたか確認
				//商品別売上額の合計が10桁を超えたか確認
				if (saleAmount  >= 10000000000L || productAmount >= 10000000000L) {
					System.out.println(AMOUNT_OVER);
					return;
				}

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

			// 支店別集計ファイル書き込み処理
			if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
				return;
			}

			// 商品別集計ファイル書き込み処理
			if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT,commodityNames, commoditySales)) {
				return;
			}
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String match, String currentFile) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルの存在確認
			if (!file.exists()) {
				System.out.println(currentFile+FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				// fileの中の値を,で分ける
				String[] items = line.split(",");

				//ファイルのフォーマット確認
				if ((items.length != 2) || (!items[0].matches(match))) {
					System.out.println(currentFile+INVALID_FORMAT);
					return false;
				}

				// Mapの値を保持する
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {

			// ファイルを開いている場合
			if (br != null) {
				try {

					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : sales.keySet()) {

				//支店コードまたは商品コード,支店名または商品名,売上金額の書き込み
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {

			// ファイルを開いている場合
			if (bw != null) {
				try {

					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
